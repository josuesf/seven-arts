import 'package:flutter/material.dart';

class Entidades extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/map.jpg"),
              fit: BoxFit.cover
            ),
          ),
          height: 100.00,
        ),
        Container(
          color: Color(0xFFF8454E),
          height: 20.0,
          alignment: Alignment(-1.0, 0.0),
          child: Text(
            "Entidades",
            style: TextStyle(
              color: Colors.white,
            ),
            textAlign: TextAlign.start,
          ),

        ),

      ]
    );
  }
  
}