import 'package:flutter/material.dart';

class HomeTab extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(children: <Widget>[
      Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/img/map.jpg"), fit: BoxFit.cover)),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              right: 3.0,
              top: 5.0
            ),
            height: 60.0,
            width: 40.0,
            color: Colors.black45,
            child: Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.youtube_searched_for,
                    color: Color(0xFF68275C),
                  ),
                ),
                Text(
                  "Descubre",
                  style: TextStyle(color: Colors.white, fontSize: 8.0),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              right: 3.0,
              top: 5.0
            ),
            height: 60.0,
            width: 40.0,
            color: Colors.black45,
            child: Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.blur_on,
                    color: Colors.red,
                  ),
                ),
                Text(
                  "Pregunta",
                  style: TextStyle(color: Colors.white, fontSize: 8.0),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              right: 3.0,
              top: 5.0
            ),
            height: 60.0,
            width: 40.0,
            color: Colors.black45,
            child: Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.train,
                    color: Color(0xFF68275C),
                  ),
                ),
                Text(
                  "Planifica",
                  style: TextStyle(color: Colors.white, fontSize: 8.0),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
             
            },
            child: Container(
              margin: EdgeInsets.only(
              right: 3.0,
              top: 5.0
            ),
              height: 60.0,
              width: 40.0,
              color: Colors.black45,
              child: Column(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.accessibility,
                      color: Colors.red,
                    ),
                  ),
                  Text(
                    "Experto",
                    style: TextStyle(color: Colors.white, fontSize: 8.0),
                  )
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              right: 3.0,
              top: 5.0
            ),
            height: 60.0,
            width: 40.0,
            color: Colors.black45,
            child: Column(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.local_library,
                    color: Color(0xFF68275C),
                  ),
                ),
                Text(
                  "Reserva",
                  style: TextStyle(color: Colors.white, fontSize: 8.0),
                )
              ],
            ),
          ),
        ],
      )
    ]);
  }
}
