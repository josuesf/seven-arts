import 'package:flutter/material.dart';
import 'components/entidad.dart';
import 'entidadDetail.dart';

class ListEntidades extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Buscador"),
      ),
      body: ListView(
        children: <Widget>[
          InkWell(
              child: Entidad("assets/img/plaza.jpg"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EntidadDetail()),
                );
              }),
          InkWell(
              child: Entidad("assets/img/santaana.jpg"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EntidadDetail()),
                );
              }),
          
        ],
      ),
    );
  }
}
