import 'package:flutter/material.dart';
import 'listEntidades.dart';

class Buscador extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: 10.0,
            right: 10.0
          ),
          child: TextFormField(
          decoration: InputDecoration(labelText: 'Buscar'),
        )),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Arquitectura")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Bellas Artes")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Diseño y Moda")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Artes Visuales")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Música")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Artes escenicas")
          ],
        ),
        Row(
          children: <Widget>[
            Checkbox(
              value: true,
              onChanged: (val) {},
            ),
            Text("Literatura")
          ],
        )
        ,
        Container(
          margin: EdgeInsets.only(
            left: 10.0,
            right: 10.0
          ),
          child: RaisedButton(
            color: Colors.red,
            textColor: Colors.white,
            child: Text("Buscar"),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListEntidades()),
              );
            },
          )
        ),
        
      ],
    );
  }
}
