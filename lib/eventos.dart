import 'package:flutter/material.dart';

class Eventos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
          color: Color(0xFFF8454E),
          height: 40.0,
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5.0),
                decoration:BoxDecoration(
                  border: Border.all(
                    color: Colors.white
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.0),
                    bottomLeft: Radius.circular(5.0)
                  )
                ),
                child: InkWell(
                  child: Text(
                    "Proximos Eventos",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10.0
                      ),
                    ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                decoration:BoxDecoration(
                  border: Border.all(
                    color: Colors.white
                  ),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0)
                  )
                ),
                child: InkWell(
                  child: Text(
                    "Eventos Finalizados",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10.0

                      ),
                    ),
                ),
              ),
            
            ],
          ),
        )

      ],
    );
  }
}
