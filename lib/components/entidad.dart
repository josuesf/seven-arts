import 'package:flutter/material.dart';

class Entidad extends StatelessWidget{
  String descripcion = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever";
  String pathImage;
  Entidad(this.pathImage);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double widthScreen = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.all(5.0),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(pathImage),
                fit: BoxFit.cover
              ),
            ),
            height: 100.0,
            width: 100.0,
            margin: EdgeInsets.only(
              right: 5.0
            ),
          ),
          Container(
            child: Text(descripcion),
            width: widthScreen-120,
          )
          
        ],
      ),
    );
  }

}