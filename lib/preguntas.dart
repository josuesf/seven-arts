import 'package:flutter/material.dart';

class Preguntas extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(5.0),
          padding: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.red
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0))
          ),
          child: Text("Pregunta ..."),
        ),
        Container(
          margin: EdgeInsets.all(5.0),
          padding: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.red
            ),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
            color: Colors.red
          ),
          child: InkWell(
            child: Text(
              "Preguntar",
              style: TextStyle(
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ), 
      ],
    );
  }

}