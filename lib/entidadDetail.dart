import 'package:flutter/material.dart';

class EntidadDetail extends StatelessWidget{
  String description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. ";
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double heightScreen = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text("Plaza Santa Ana"),
      ),
      body:Container(
        height: heightScreen-50,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/img/plaza.jpg"),
          fit: BoxFit.cover
        )
      ),
      child: Container(
        padding: EdgeInsets.all(10.0),
        color: Colors.black38,
        child: Text(
          description,
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0
          ),
        ),
      ),
    ) 
    );
  }

}