import 'package:flutter/material.dart';
import 'drawer.dart';
import 'home.dart';
import 'entidades.dart';
import 'eventos.dart';
import 'mapaView.dart';
import 'buscador.dart';
import 'preguntas.dart';
import 'dart:async';

void main() => runApp(MyApp());

class SplashPage extends StatefulWidget {
  @override
  Splash createState() => Splash();
}

class Splash extends State<SplashPage> {
  void navigationToNextPage() {
    Navigator.pushReplacementNamed(context, '/HomePage');
  }

  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {
    super.initState();
    startSplashScreenTimer();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Color(0xFFF8454E),
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("assets/img/seven.png"))),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Seven Arts',
      theme: ThemeData(
        primaryColor: Color(0xFFF8454E),
        primarySwatch: Colors.red,
        indicatorColor: Colors.white,
      ),
      home: SplashPage(),
      routes: <String, WidgetBuilder>{
        '/HomePage': (BuildContext context) =>
            new MyHomePage(title: "Seven Arts")
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _showDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Recomendacion del Experto"),
          content: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Radio(
                    value: 0,
                  ),
                  Text("Patrimonio Artistico"),
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 40.0),
                child: Text(
                  "(patrimonio turistico,jardines historicos,sitios historicos, restos arqueologicos)",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black45,
                  ),
                ),
              ),
              Row(children: <Widget>[
                Radio(
                  value: 0,
                ),
                Text("Patrimonio Natural"),
                Text(
                  "",
                  style: TextStyle(fontSize: 10, color: Colors.black45),
                )
              ]),
              Container(
                margin: EdgeInsets.only(left: 40.0),
                child: Text(
                  "(reserva biosferica,monumentos naturales,reservas nacionales, parques naturales)",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black45,
                  ),
                ),
              ),
              Row(children: <Widget>[
                Radio(
                  value: 1,
                  groupValue: 1,
                  onChanged: null,
                ),
                Text("Mixto"),
                Text(
                  "",
                  style: TextStyle(fontSize: 10, color: Colors.black45),
                )
              ])
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showDialog2(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Reserva"),
          content: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Radio(
                    value: 0,
                  ),
                  Text("Personalizado"),
                ],
              ),
              Row(children: <Widget>[
                Radio(
                  value: 0,
                ),
                Text("Planificado"),
                Text(
                  "",
                  style: TextStyle(fontSize: 10, color: Colors.black45),
                )
              ]),
              Row(children: <Widget>[
                Radio(
                  value: 1,
                  groupValue: 1,
                  onChanged: null,
                ),
                Text("Recomienda el experto"),
                Text(
                  "",
                  style: TextStyle(fontSize: 10, color: Colors.black45),
                )
              ])
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.accessibility),
            onPressed: () {
              _showDialog(context);
            },
          ),
          IconButton(
            icon: Icon(Icons.local_library),
            onPressed: () {
              _showDialog2(context);
            },
          ),
        ],
      ),
      resizeToAvoidBottomPadding: false,
      body: DefaultTabController(
        length: 6,
        child: Scaffold(
          appBar: AppBar(
            // bottom: ,
            title: TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.home),
                ),
                Tab(icon: Icon(
                  Icons.youtube_searched_for,
                  color: Color(0xFF68275C),
                )),
                Tab(icon: Icon(Icons.calendar_today)),
                // Tab(icon: Icon(Icons.pin_drop)),
                Tab(icon: Icon(Icons.train,color: Color(0xFF68275C),)),
                Tab(icon: Icon(Icons.blur_on)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              HomeTab(),
              Entidades(),
              Eventos(),
              // MapView(),
              Buscador(),
              Preguntas(),
            ],
          ),
        ),
      ),
      drawer: DrawerNav(),
    );
  }
}
